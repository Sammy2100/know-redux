import { createStore, combineReducers, compose, applyMiddleware } from "redux";
import userReducer from "./reducer/user-reducer";
import productReducer from "./reducer/product-reducer";
import thunk from "redux-thunk";

const allReducers = combineReducers({
  users: userReducer,
  products: productReducer
});

//const initialState = { user: "Samuel", products: {} };

const allStoreEnhancers = compose(
  applyMiddleware(thunk),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

const store = createStore(allReducers, allStoreEnhancers);

export default store;
