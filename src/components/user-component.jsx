import React, { Component } from "react";
import { addUser, updateUser, deleteUser } from "../action/user-action";
import { connect } from "react-redux";
import { createSelector } from "reselect";

class User extends Component {
  onAddUser = event => {
    const user = {
      id: 4,
      fullname: "Diamond Phillip",
      role: "Software Developer"
    };

    this.props.onAddUser(user);
    // this.props.onUpdateUser(event.target.value);
  };

  onUpdateUser = event => {
    const user = {
      id: 4,
      fullname: "Iyimbo Joshua",
      role: "Software Engineer"
    };

    this.props.onUpdateUser(user);
  };

  onDeleteUser = id => {
    this.props.onDeleteUser(3);
  };

  render() {
    console.log(this.props);
    return (
      <div>
        <div>
          <input onChange={this.onUpdateUser} />
        </div>
        <h1>Users</h1>
        <button onClick={this.onAddUser}>Add user</button>
        <button onClick={this.onUpdateUser}>Update user</button>
        <div>
          {this.props.users.map(user => (
            <div onDoubleClick={() => this.onDeleteUser(user.id)} key={user.id}>
              {user.fullname} | {user.role}
            </div>
          ))}
        </div>
        <hr />
      </div>
    );
  }
}

const mapStateToPropsOld = state => ({
  users: state.users
});

const userSelector = createSelector(
  state => state.users,
  users => users
);

const mapStateToProps = createSelector(userSelector, users => ({
  users
}));

const mapActionsToProps = {
  onAddUser: addUser,
  onUpdateUser: updateUser,
  onDeleteUser: deleteUser
};

export default connect(mapStateToProps, mapActionsToProps)(User);
