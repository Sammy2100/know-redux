import React, { useState } from "react";
import Todo from "./todo";

const TodoLists = () => {
  const [todos, setTodos] = useState({
    todos: ["Eat", "Sleep", "Code"]
  });

  const onUpdateTodos = () => {
    const newTodos = ["Read", "Exercise", "Swim"];
    setTodos({ todos: newTodos });
  };

  return (
    <div>
      {todos.todos.map((todo, index) => (
        <Todo task={todo} key={index} />
      ))}

      <button onClick={onUpdateTodos}>Update Todos</button>
    </div>
  );
};

export default TodoLists;
