import React, { Component } from "react";
import Todo from "./todo";

class TodoList extends Component {
  state = {
    todos: ["Eat", "Sleep", "Code"]
  };

  render() {
    return (
      <div>
        {this.state.todos.map((todo, index) => (
          <Todo task={todo} key={index} />
        ))}
      </div>
    );
  }
}

export default TodoList;
