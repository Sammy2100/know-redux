import React, { Component } from "react";
import {
  updateProduct,
  deleteProduct,
  addProduct
} from "../action/product-action";
import { connect } from "react-redux";
import { createSelector } from "reselect";

class Product extends Component {
  onAddProduct = () => {
    const product = { id: "p5", title: "Pc Motherboard for HP", price: 94.67 };
    this.props.onAddProduct(product);
  };

  onUpdateProduct = () => {
    const product = { id: "p1", title: "No more Gaming Mouse", price: 99.99 };
    this.props.onUpdateProduct(product);
  };

  onDeleteProduct = productId => {
    this.props.onDeleteProduct(productId);
  };

  render() {
    return (
      <div>
        <h1>Products</h1>
        <button onClick={this.onAddProduct}>Add product</button>
        <button onClick={this.onUpdateProduct}>Update product</button>
        {this.props.products &&
          this.props.products.map(product => (
            <div
              onDoubleClick={() => this.onDeleteProduct(product.id)}
              key={product.id}
            >
              {product.title}
            </div>
          ))}
        <hr />
      </div>
    );
  }
}

const mapStateToPropsOld = state => ({
  products: state.products
});

const productSelector = createSelector(
  state => state.products,
  products => products
);

const mapStateToProps = createSelector(productSelector, products => ({
  products
}));

const mapActionsToProps = {
  onAddProduct: addProduct,
  onUpdateProduct: updateProduct,
  onDeleteProduct: deleteProduct
};

export default connect(mapStateToProps, mapActionsToProps)(Product);
