export const addData = (dataStore, dataToAdd) => {
  const newDataStore = [...dataStore];
  newDataStore.push(dataToAdd);
  return newDataStore;
};

export const updateData = (dataStore, dataUpdate) => {
  const newDataStore = [...dataStore];
  const index = newDataStore.findIndex(p => p.id === dataUpdate.id);
  newDataStore[index] = dataUpdate;
  return newDataStore;
};

export const deleteData = (dataStore, id) => {
  const data = [...dataStore];
  const newDataStore = data.filter(p => p.id !== id);
  return newDataStore;
};
