export const ADD_PRODUCT = "product:addProduct";
export const UPDATE_PRODUCT = "product:updateProduct";
export const DELETE_PRODUCT = "product:deleteProduct";

export function addProduct(product) {
  return {
    type: ADD_PRODUCT,
    payload: { product: product }
  };
}

export function updateProduct(product) {
  return {
    type: UPDATE_PRODUCT,
    payload: { product: product }
  };
}

export function deleteProduct(productId) {
  return {
    type: DELETE_PRODUCT,
    payload: { productId }
  };
}
