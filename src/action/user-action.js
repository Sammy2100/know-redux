export const ADD_USER = "users:updateUser";
export const UPDATE_USER = "users:updateUser";
export const DELETE_USER = "users:updateUser";

export function addUser(user) {
  return {
    type: ADD_USER,
    payload: { user: user }
  };
}
export function updateUser(user) {
  return {
    type: UPDATE_USER,
    payload: { user: user }
  };
}
export function deleteUser(id) {
  return {
    type: DELETE_USER,
    payload: { id }
  };
}
