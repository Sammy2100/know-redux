import React from "react";
import "./App.css";
import User from "./components/user-component";
import Product from "./components/product-component";

function App() {
  return (
    <div className="App">
      <User />
      <Product />
      {/* <hr />
      <TodoList />
      <hr />
      <TodoLists /> */}
    </div>
  );
}

export default App;
