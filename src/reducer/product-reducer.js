import {
  ADD_PRODUCT,
  UPDATE_PRODUCT,
  DELETE_PRODUCT
} from "../action/product-action";

const initialState = [
  { id: "p1", title: "Gaming Mouse", price: 29.99 },
  { id: "p2", title: "Harry Potter 3", price: 9.99 },
  { id: "p3", title: "Used plastic bottle", price: 0.99 },
  { id: "p4", title: "Half-dried plant", price: 2.99 }
];
export default function productReducer(
  state = initialState,
  { type, payload }
) {
  switch (type) {
    case ADD_PRODUCT:
      return addProduct(state, payload);

    case UPDATE_PRODUCT:
      return updateProduct(state, payload);

    case DELETE_PRODUCT:
      return deleteProduct(state, payload);

    default:
      return state;
  }
}

const addProduct = (state, payload) => {
  const addedProduct = [...state];
  addedProduct.push(payload.product);
  return addedProduct;
};

const updateProduct = (state, payload) => {
  const updatedProducts = [...state];
  const index = updatedProducts.findIndex(p => p.id === payload.product.id);
  updatedProducts[index] = payload.product;
  return updatedProducts;
};

const deleteProduct = (state, payload) => {
  console.log(payload);
  const data = [...state];
  const newProducts = data.filter(p => p.id !== payload.productId);
  return newProducts;
};
