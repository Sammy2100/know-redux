import { UPDATE_USER, ADD_USER, DELETE_USER } from "../action/user-action";
import { addData, updateData, deleteData } from "../common/crud";

const initialState = [
  { id: 1, fullname: "Iyomere Samuel", role: "Snr. Software Engineer" },
  { id: 2, fullname: "Eyaefe Success", role: "Human Resource" },
  { id: 3, fullname: "Odhigbo Ewomazino", role: "UI/UX Engineer" }
];

export default function userReducer(state = initialState, { type, payload }) {
  switch (type) {
    case ADD_USER:
      return addData(state, payload.user);
    case UPDATE_USER:
      return updateData(state, payload.user);
    case DELETE_USER:
      return deleteData(state, payload.id);
    default:
      console.log(state);
      return state;
  }
}
